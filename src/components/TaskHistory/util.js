import {parseDate} from "../../services/util";

export const createCalendarBoxes = (dateDetails) => {
  const boxes = [];
  for (let i = 0; i < dateDetails?.startDay?.dayIndex; i++) {
    boxes.push({});
  }
  for (let i = dateDetails?.startDay?.day; i <= dateDetails?.endDay?.day; i++) {
    boxes.push({date: parseDate(dateDetails.year, dateDetails.month, i)});
  }
  for (let i = dateDetails?.endDay?.dayIndex; i < 6; i++) {
    boxes.push({});
  }
  return boxes;
}
