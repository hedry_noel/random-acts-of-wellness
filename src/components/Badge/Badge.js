const {children} = require("solid-js");

const Badge = props => {
  const c = children(() => props.children);

  return (
    <div class={`badge font-xs u-flex u-align-center u-justify-center ${props.class}`}>{c()}</div>
  )
}

export default Badge;
