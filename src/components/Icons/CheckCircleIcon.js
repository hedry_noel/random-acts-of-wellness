import {COLOR_GOOD} from "../../constants";

const CheckCircleIcon = (props) => <span class={`material-icons ${props.size ? props.size : 'md-36'}`} style={{color: props.color || COLOR_GOOD}}>task_alt</span>

export default CheckCircleIcon;
