import CheckboxInput from "../Inputs/CheckboxInput";
import Card from "../../containers/Card";
import {createTaskStorage} from "../../hooks/createTaskStorage";
import {taskToStorage} from "../../services/util";
import {pipe} from "ramda";

const TaskItem = props => {
  const {updateTask} = createTaskStorage();

  const setCompleted = () => {
    props.setTask({...props.task(), completed: !props.task().completed});
    return pipe(
      taskToStorage,
      updateTask
    )(props.task());
  }
  const handleKeyPress = e => {
    const {keyCode} = e;
    if (keyCode === 13) {
      setCompleted();
    }
  }

  return (
    <Card class={'font-base font-md task__item u-m-auto u-mb-5 u-py-5 u-px-6'}>
      <p class={"font-md"}>Your random act of wellness for today is: </p>
      <p class={'font-headline font-lg u-my-4'}>{props.task()?.label}</p>
      <p>{props.task()?.description}</p>
      <CheckboxInput checked={props.task().completed} class={'u-pt-4'} onChange={() => setCompleted()}
                     onKeyPress={handleKeyPress}
                     id={'task-completed'}>Complete?</CheckboxInput>
    </Card>
  )
}

export default TaskItem
