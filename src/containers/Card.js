import {children} from "solid-js";

const Card = (props) => {
  const c = children(()=> props.children);
  return (
    <div class={`card ${props.class}`} {...props}>
      {c()}
    </div>
  )
}

export default Card;
