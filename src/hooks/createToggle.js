import {createSignal} from "solid-js";

export const createToggle = init => {
  const [toggle, setToggle] = createSignal(init ?? false);

  const toggleValue = (next) => {
    next ? setToggle(next) : setToggle(prev => !prev);
  }

  return [toggle, toggleValue];
}
