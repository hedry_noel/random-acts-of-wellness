import CheckCircleIcon from "../Icons/CheckCircleIcon";
import XCircleIcon from "../Icons/XCircleIcon";

const TaskHistoryCompleted = (props) => {
  if (!props.task()?.date) return null;
  return (
    <>
      {props.task()?.completed ? <CheckCircleIcon size={'md-24'}/> : <XCircleIcon size={"md-24"}/>}
      <p class={'font-sm'}>{props.task()?.completed ? 'Done' : 'Not Done'}</p>
    </>
  )

}

export default TaskHistoryCompleted;
