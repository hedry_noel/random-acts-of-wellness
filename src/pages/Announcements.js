import Card from "../containers/Card";
import {For} from "solid-js";
import AnnouncementItem from "../components/Announcement/AnnouncementItem";

const Announcements = (props) => (
  <div className={"container font-base u-mt-5 u-m-auto"}>
    <Card class={'font-headline font-xl text-center u-m-auto u-mb-5 u-py-3 u-px-4'}>
      <h3 className={'u-mb-5 u-pt-5'}>Announcements</h3>
    </Card>
    <For each={props.announcements()}>{announcement => <AnnouncementItem toggleSeen={props.toggleSeen} announcement={announcement}/>}</For>
  </div>
)

export default Announcements;
