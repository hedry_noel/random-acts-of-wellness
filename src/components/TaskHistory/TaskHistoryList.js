import {endOfMonth, startOfMonth} from "date-fns";
import {getDateDetails, getTaskById} from "../../services/util";
import {DAYS_OF_WEEK, MONTHS} from "../../constants";
import Card from "../../containers/Card";
import {createCalendarBoxes} from "./util";
import TaskHistoryItem from "./TaskHistoryItem";
import {wellnessTasks} from "../../services/wellnessTasks";
import {Show} from 'solid-js';
import PrintIcon from "../Icons/PrintIcon";

const TaskHistoryList = (props) => {
  const {month, year, tasks} = props.history;
  const date = new Date(year, month);
  const monthStart = startOfMonth(date)
  const monthEnd = endOfMonth(date)
  const dateDetails = getDateDetails(monthStart, monthEnd);
  const calendarBoxes = createCalendarBoxes(dateDetails);

  return (
    <div class={"container font-base page__print u-mt-5 u-m-auto"}>
      <Card class={'calendar__month font-headline font-xl text-center u-m-auto u-mb-5 u-px-4 u-py-3 u-relative'}>
        <h3 class={'u-mt-5 u-py-3'}>History for {MONTHS[dateDetails.month]}</h3>
        <button
          class={'button__print font-base u-absolute u-flex u-flex-row u-align-center u-justify-center u-pointer u-print-none u-m-3 u-px-3 u-py-2 u-right-0 u-top-0'}
          onClick={() => window.print()}>
            <PrintIcon color="white" size={'md-24'} />
            <span class={'u-ml-3'}>Export</span>
        </button>
      </Card>
      <div class={"calendar u-shadow-lg"}>
        <For each={DAYS_OF_WEEK}>{day =>
          <p class={"font-base font-bold text-center u-hidden-md u-print-none u-py-3"}>{day}</p>
        }</For>
        <Show when={props.task()}>
          <For each={calendarBoxes}>{calendarBox => {
            const taskFromHistory = tasks?.find(historyTask => historyTask.date === calendarBox.date);
            return (
              <TaskHistoryItem calendarBox={calendarBox} task={() => ({
                ...taskFromHistory, ...getTaskById(taskFromHistory?.id)(wellnessTasks)
              })}/>
            )
          }}</For>
        </Show>
      </div>
    </div>
  )
}

export default TaskHistoryList;
