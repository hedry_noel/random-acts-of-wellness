import TaskHistoryCompleted from "./TaskHistoryCompleted";

const TaskHistoryItem = props => {
  return (
    <div
      class={"calendar__box u-flex u-flex-col u-align-start u-justify-between u-px-3 u-py-3"}
      classList={{'u-hidden-md': !props.calendarBox?.date}}>
      <div>
        <p className={'calendar__box--date'}>{props.task()?.date ?? props.calendarBox?.date}</p>
        <p className={'calendar__box--task'}>{props.task()?.label}</p>
      </div>
      <div class={'u-flex u-flex-row u-align-center u-justify-center u-m-auto'}>
        <TaskHistoryCompleted task={props.task}/>
      </div>
    </div>
  )
}

export default TaskHistoryItem;
