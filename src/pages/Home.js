import DateCard from "../components/DateCard";
import {createSignal, Show} from "solid-js";
import TaskItem from "../components/Task/TaskItem";
import Randomizer from "../components/Task/Randomizer";
import {add, format, sub} from "date-fns";
import {DATE_FORMAT} from "../constants";
import {createTaskStorage, getHistoryKeys} from "../hooks/createTaskStorage";
import {equals, find, propEq} from "ramda";
import {getTaskDetails, today} from "../services/util";
import Card from "../containers/Card";

const NEXT = 'NEXT';
const PREV = 'PREV';

const Home = (props) => {
  const {getHistoryById} = createTaskStorage();
  const [date, setDate] = createSignal(new Date());

  const handleTaskChange = (dir = NEXT) => {
    const nextDate = equals(NEXT, dir) ? add(date(), {days: 1}) : sub(date(), {days: 1});
    const {key} = getHistoryKeys(date());
    const history = getHistoryById(key)
    const nextTask = find(propEq('date', nextDate.toLocaleDateString()), history.tasks);
    if (nextTask) {
      setDate(nextDate);
      props.setTask({...nextTask, ...getTaskDetails(nextTask)});
    }
  }

  return (
    <>
      <div class={'u-flex u-flex-row u-align-center u-justify-evenly'}>
        <Card class="u-p-3 u-pointer u-select-none" onClick={() => handleTaskChange(PREV)}>Prev</Card>
        <DateCard date={format(date(), DATE_FORMAT)}/>
        <Card class="u-p-3 u-pointer u-select-none" onClick={() => handleTaskChange(NEXT)}>Next</Card>
      </div>
      <Show when={!!props.task()} fallback={<p>Loading...</p>}>
        <TaskItem task={props.task} setTask={props.setTask}/>
      </Show>
      <Show when={props.task() && props.task().date === today()}>
        <Randomizer setTask={props.setTask}/>
      </Show>
    </>
  )
}
export default Home;
