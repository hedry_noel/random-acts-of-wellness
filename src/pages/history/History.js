import Card from "../../containers/Card";
import {createTaskStorage} from "../../hooks/createTaskStorage";
import {createSignal, For} from "solid-js";
import {Link} from "solid-app-router";
import {MONTHS} from "../../constants";

const History = () => {
  const {getHistory} = createTaskStorage();
  const [history] = createSignal(getHistory());

  return (
    <div className={"container font-base u-mt-5 u-m-auto"}>
      <Card class={'font-headline font-xl text-center u-m-auto u-mb-5 u-py-3 u-px-4'}>
        <h3 className={'u-mb-5 u-py-3'}>History</h3>
        <div className={"u-flex u-flex-row u-align-center u-justify-evenly u-mb-4"}>
          <For each={Object.entries(history())}>{([k, v]) => <Link className={'font-md'} href={`/history/${k}`}>{MONTHS[v.month]}, {v.year}</Link>}
          </For>
        </div>
      </Card>
    </div>
  )
}

export default History;
