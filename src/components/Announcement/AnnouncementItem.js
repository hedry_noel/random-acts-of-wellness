import Card from "../../containers/Card";
import {isDate, parseISO} from "date-fns";

const renderDate = dt => {
  if (isDate(dt)) {
    return dt.toLocaleDateString();
  }
  return parseISO(dt).toLocaleDateString();
}

const AnnouncementItem = props => {
  return (
    <Card class={'u-m-auto u-mb-3 u-pb-3 u-pt-3 u-pl-2 u-pr-2'}>
      <div class={"announcement__item"} style={{opacity: props.announcement.seen ? 0.4 : 1}}>
        <span class={"announcement__item--date font-md"}><p>{renderDate(props.announcement?.date)}</p></span>
        <div class={"u-flex"}>
          <p class={'font-bold u-mb-3'}>{props.announcement.title}</p>
          <span>{props.announcement.message}</span>
          <p class="font-bold u-mt-3 u-pointer" role={"button"}
             onClick={() => props.toggleSeen(props.announcement.id)}>Dismiss</p>
        </div>
      </div>
    </Card>
  )
}

export default AnnouncementItem;
