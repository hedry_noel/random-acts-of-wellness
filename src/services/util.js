import {format} from "date-fns";
import {DAYS_OF_WEEK} from "../constants";
import {__, curry, find, flip, pick, pipe, prop, propEq} from "ramda";
import {wellnessTasks} from "./wellnessTasks";

export const taskToStorage = pick(['id', 'date', 'completed']);

export const log = curry((l, v) => {
  console.log(l, v);
  return v;
})

export function random(min, max) {
  return function nextRandom(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
  }
}

export function range(start, end) {
  const r = [];
  for(let i = start; i < end; i++) {
    r.push(i);
  }
  return r;
}

export function between(start) {
  return function betweenEnd(end) {
    return function betweenTest(val) {
      return start <= val && val < end;
    }
  }
}

export const getTaskById = (id) => (tasks) => tasks.find(task => Number(task.id) === Number(id));
export const getTaskDetails = pipe(
  prop('id'),
  Number,
  v => find(propEq('id', v), wellnessTasks)
)

export const getDateDetails = (startDate, endDate) => ({
  month: startDate.getMonth(),
  year: startDate.getFullYear(),
  startDay: {
    day: format(startDate, 'd'),
    dayOfMonth: format(endDate, 'eeee'),
    dayIndex: DAYS_OF_WEEK.indexOf(format(startDate, 'eeee'))
  },
  endDay: {
    day: format(endDate, 'd'),
    dayOfMonth: format(endDate, 'eeee'),
    dayIndex: DAYS_OF_WEEK.indexOf(format(endDate, 'eeee'))
  }
})

export const parseDate = (year, month, day) => new Date(year, month, day).toLocaleDateString();

export const today = () => new Date().toLocaleDateString();

export function isObject(obj) {
  return Object.prototype.toString.call(obj) === '[object Object]';
}
