import { useLocalStorage } from './useLocalStorage';
import { createRandomTask } from '../services/wellnessTasks';
import { isObject } from '../services/util';
import { MONTHS } from '../constants';
import { find, map, pipe, prop, propEq, propOr } from 'ramda';

export const getHistoryKeys = date => {
	const jsDate = new Date(date);
	const month = jsDate.getMonth();
	const year = jsDate.getFullYear();
	const key = `${MONTHS[month]}${year}`;
	return { month, year, key };
};

export const createTaskStorage = () => {
	const localStorage = useLocalStorage();

	const addToHistory = task => {
		const { id, date, completed } = task;
		const { month, year, key } = getHistoryKeys(date);
		const taskHistory = getHistory();
		if (!taskHistory || taskHistory === {}) {
			createHistory({
				[key]: {
					month,
					year,
					tasks: [{ id, date, completed }]
				}
			});
		} else {
			localStorage.set('taskHistory', {
				...taskHistory,
				[key]: {
					...taskHistory[key],
					month,
					year,
					tasks: taskHistory[key]?.tasks
						? [...taskHistory[key].tasks, { id, date, completed }]
						: [{ id, date, completed }]
				}
			});
		}
		return task;
	};

	const createAndStoreTask = pipe(createRandomTask, addToHistory);

	const createHistory = history => localStorage.set('taskHistory', history);
	const createHistoryBackup = history =>
		localStorage.set('taskHistoryBackup', history);
	const getHistory = () => localStorage.get('taskHistory');
	const getHistoryById = historyId => {
		const fullHistory = localStorage.get('taskHistory');
		return propOr({}, historyId)(fullHistory);
	};
	const getTask = taskDate =>
		pipe(getHistoryById, propOr([], 'tasks'), find(propEq('date', taskDate)));
	const updateTask = newTask => {
		const history = getHistory();
		const { key } = getHistoryKeys(newTask.date);
		const historyItem = getHistoryById(key);
		const historyItemTasks = prop('tasks')(historyItem);
		const updatedHistoryItemTasks = map(
			task => (task.date === newTask.date ? newTask : task),
			historyItemTasks
		);
		return createHistory({
			...history,
			[key]: { ...historyItem, tasks: updatedHistoryItemTasks }
		});
	};

	const migrateTaskHistory = () => {
		const history = getHistory();
		if (history && !isObject(history)) {
			const migratedHistory = history.reduce((acc, next) => {
				const { month, year, key } = getHistoryKeys(next.date);
				return {
					...acc,
					[key]: {
						month,
						year,
						tasks: acc[key]?.tasks ? [...acc[key]?.tasks, next] : [next]
					}
				};
			}, {});
			createHistoryBackup(history);
			createHistory(migratedHistory);
		}
	};

	return {
		addToHistory,
		createAndStoreTask,
		createHistory,
		getHistory,
		getHistoryById,
		getTask,
		migrateTaskHistory,
		updateTask
	};
};
