export const MONTHS = [
  'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
]
export const DAYS_OF_WEEK = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
export const DATE_FORMAT = 'eee, MMMM d';
export const COLOR_PRIMARY = '#FF914D';
export const COLOR_SECONDARY = '#61ABD1';
export const COLOR_SECONDARY_LIGHT = '#A1BEE8';
export const COLOR_SECONDARY_DARK = '#386375';
export const COLOR_GOOD = '#48946e';
export const COLOR_BAD = '#A63D40';
