import {COLOR_BAD} from "../../constants";

const XCircleIcon = props => <span class={`material-icons ${props.size ? props.size : 'md-36'}`} style={{color: props.color || COLOR_BAD}}>highlight_off</span>

export default XCircleIcon
