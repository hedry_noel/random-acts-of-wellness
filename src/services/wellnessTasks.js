export const wellnessTasks = [
  {id: 1, label: 'Early 30', description: 'Wake up 30 minutes early each day and do something productive.'},
  {id: 2, label: 'Limit Screen Time', description: 'Limit screen time to 1 hour a day beyond work time.'},
  {
    id: 3,
    label: '30 Minutes of Me Time',
    description: 'Run, read, do yoga, meditate, work out, bake, go to bed early, etc.'
  },
  {id: 4, label: 'Do the "Noon Walk"', description: 'Walk for 30 minutes over the lunch hour or any hour of the day.'},
  {
    id: 5,
    label: 'Socialize',
    description: 'Catch up with a friend or family member by giving them a call for 15 minutes. Reach out to someone different each day you choose this option.'
  },
  {
    id: 6,
    label: 'Finish Something',
    description: 'De-clutter your space or finish a project. Clean out different areas of your house, car, or general work space.'
  },
  {id: 7, label: 'Journal', description: 'Start a gratitude journal and journal 15 minutes a day.'},
  {
    id: 8,
    label: 'Say "No"',
    description: 'Say "No" to something that you would usually say "Yes" to, but that causes you stress or anxiety.'
  },
  {
    id: 9,
    label: 'Say "Yes"',
    description: 'Say "Yes" to something that you would typically say "No" to because you don\'t have energy, but know it would be a positive experience for you.'
  },
  {
    id: 10,
    label: 'Purge Stress',
    description: 'Remove something or someone that brings unhealthy stress to your life.'
  },
]

export const createRandomTask = () => ({
  ...wellnessTasks[Math.floor(Math.random() * wellnessTasks.length)], date: new Date().toLocaleDateString(), completed: false
})

export const createTasks = () => {
  const weeks = [
    [{}, {}, {}, {}, {date: 1}, {date: 2}, {date: 3}],
    [{date: 4}, {date: 5}, {date: 6}, {date: 7}, {date: 8}, {date: 9}, {date: 10}],
    [{date: 11}, {date: 12}, {date: 13}, {date: 14}, {date: 15}, {date: 16}, {date: 17}],
    [{date: 18}, {date: 19}, {date: 20}, {date: 21}, {date: 22}, {date: 23}, {date: 24}],
    [{date: 25}, {date: 26}, {date: 27}, {date: 28}, {date: 29}, {date: 30}, {}]
  ];
  const _weeks = weeks.map(week =>
    week.map(entry => {
      if (entry.date) {
        return {...entry, ...createRandomTask()}
      }
      return entry
    })
  );
  return _weeks;
}
