import {children} from "solid-js";
import XCircleIcon from "../Icons/XCircleIcon";

const AnnouncementContainer = (props) => {
  const c = children(() => props.children);

  return (
    <div class={'modal u-fixed'}>
      <div class={"modal-inner"}>
        <header>
          <h2 class={"font-headline font-lg text-center"}>Announcements</h2>
          <span class={'u-absolute u-mr-3 u-mt-3 u-pointer u-right-0 u-top-0'}>
            <XCircleIcon />
          </span>
        </header>
        <section>
          {c()}
        </section>
      </div>
    </div>
  )
}

export default AnnouncementContainer;
