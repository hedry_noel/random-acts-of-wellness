import {children} from "solid-js";

const CheckboxInput = props => {
  const c = children(() => props.children);
  return (
    <div class={`input__checkbox u-flex u-flex-row u-align-center u-justify-center ${props.class}`}>
      <input
        aria-required={false}
        checked={props.checked}
        class={`input__checkbox--actual u-px-3`}
        disabled={props.disabled}
        id={props.id}
        type={'checkbox'}
        onChange={props.onChange}
      />
      <label for={props.id} class={'font-md input__checkbox--label u-ml-4'}>{c()}</label>
    </div>
  )
}

export default CheckboxInput;
