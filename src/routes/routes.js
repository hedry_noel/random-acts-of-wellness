import { lazy } from 'solid-js';

const routes = [
  {
    exact: true,
    path: '/',
    component: lazy((props) => import('../pages/Home.js'))
  },
  {
    path: '/history',
    component: lazy((props) => import('../pages/history/History.js')),
  },
  {
    path: '/history/:historyItemId',
    component: lazy((props) => import('../pages/history/HistoryItem.js')),
  },
  {
    path: '/announcements',
    component: lazy((props) => import('../pages/Announcements'))
  },
]

export default routes;
