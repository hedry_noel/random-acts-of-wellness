import {COLOR_GOOD} from "../../constants";

const PrintIcon = (props) => (
  <span className={`material-icons ${props.size ? props.size : 'md-36'}`}
        style={{color: props.color || COLOR_GOOD}}>print</span>
)

export default PrintIcon;
