export const useLocalStorage = () => {
  if(!window.localStorage) {
    throw new Error('LocalStorage is not available');
  }

  return {
    check: (key) => !!window.localStorage.getItem(key),
    get: (key) => JSON.parse(window.localStorage.getItem(key)),
    removeItem: (key) => window.localStorage.removeItem(key),
    set: (key, value) => window.localStorage.setItem(key, JSON.stringify(value)),
    clear: () => window.localStorage.clear(),
  }
}
