import {createTaskStorage} from "../../hooks/createTaskStorage";
import TaskHistoryList from "../../components/TaskHistory/TaskHistoryList";
import {useParams} from "solid-app-router";

const HistoryItem = (props) => {
  const params = useParams();
  const {getHistoryById} = createTaskStorage();
  const history = getHistoryById(params.historyId);

  return (
    <TaskHistoryList historyId={params.historyId} task={props.task} history={history}/>
  )
}

export default HistoryItem;
