import {createSignal, onMount} from "solid-js";
import {wellnessTasks} from "./services/wellnessTasks";
import {createTaskStorage, getHistoryKeys} from "./hooks/createTaskStorage";
import {Route, Routes} from 'solid-app-router';
import Home from "./pages/Home";
import Announcements from "./pages/Announcements";
import HistoryItem from "./pages/history/HistoryItem";
import History from "./pages/history/History";
import Header from "./components/Header/Header";
import {getTaskDetails, isObject} from "./services/util";
import createAnnouncementsStore from "./stores/createAnnouncementsStore";
import {filter, length, not, pipe, prop} from "ramda";

const isSeen = pipe(
  prop('seen'),
  not
)
const announcementCount = pipe(
  filter(isSeen),
  length
)

function App() {
  const [announcements, toggleSeen] = createAnnouncementsStore();
  const [task, setTask] = createSignal(null);
  const {
    createAndStoreTask,
    getHistory,
    getTask,
    migrateTaskHistory
  } = createTaskStorage();

  onMount(() => {
    const history = getHistory();
    if (!isObject(history)) {
      migrateTaskHistory();
    }
    const today = new Date().toLocaleDateString();
    const {key} = getHistoryKeys(today);
    const taskFromHistory = getTask(today)(key);
    if (taskFromHistory) {
      const _task = getTaskDetails(taskFromHistory);
      setTask({..._task, date: taskFromHistory.date, completed: taskFromHistory.completed});
    } else {
      setTask(createAndStoreTask());
    }
  })

  return (
    <>
      <Header announcementCount={announcementCount(announcements())}/>
      <Routes>
        <Route path={"/"} element={<Home task={task} setTask={setTask}/>}/>
        <Route path={"/history"} element={<History/>}/>
        <Route path={"/history/:historyId"} element={<HistoryItem task={task}/>}/>
        <Route path={"/announcements"}
               element={<Announcements toggleSeen={toggleSeen} announcements={announcements}/>}/>
      </Routes>
    </>
  );
}

export default App;
