import Card from "../containers/Card";

const DateCard = (props) => (
  <Card class={'date font-base text-center u-my-5'}>
    <span class={"font-lg"}>{props.date}</span>
  </Card>
)

export default DateCard;
