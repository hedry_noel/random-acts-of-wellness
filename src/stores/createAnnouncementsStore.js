import {createEffect, createSignal} from "solid-js";
import {useLocalStorage} from "../hooks/useLocalStorage";
import {find, map, mergeRight, pipe, prop, reverse, sort, sortBy} from 'ramda';

const ANNOUNCEMENTS = [
  {
    id: 1,
    date: new Date(2021, 9, 8),
    title: 'Introducing History!',
    message: "Now you can review your progress for any month that you've used the app!"
  },
  {
    id: 2,
    date: new Date(2021, 9, 8),
    title: 'Introducing Announcements!',
    message: "Now I can tell you about new things on offer!"
  },
  {
    id: 3,
    date: new Date(2021, 9, 16),
    title: 'Improved Announcements!',
    message: "Mark announcements as seen."
  },
  {
    id: 4,
    date: new Date(2021, 9, 17),
    title: 'Update Previous Entries',
    message: "You can now update entries from previous days, in case you forgot to mark a task as completed."
  },
  {
    id: 5,
    date: new Date(2021, 9, 25),
    title: 'Export to PDF',
    message: "You can now print or export your monthly history."
  }
]

const mergeAnnouncements = (announcements, announcementsHistory) => pipe(
  map(anc =>
    pipe(
      find(ancHist => ancHist.id === anc.id),
      mergeRight(anc)
    )(announcementsHistory)),
  sortBy(prop('date')),
  reverse
)(announcements);

const createAnnouncementsStore = () => {
  const localStorage = useLocalStorage();
  const announcementsFromStorage = localStorage.get('announcements') ?? [];
  const [announcements, setAnnouncements] = createSignal(mergeAnnouncements(ANNOUNCEMENTS, announcementsFromStorage));

  createEffect(() => localStorage.set('announcements', announcements()));

  const toggleSeen = (id) => {
    const newAnnouncements = announcements().map(anc => anc.id === id ? {...anc, seen: !anc.seen ?? true} : anc);
    setAnnouncements(newAnnouncements)
  }

  return [announcements, toggleSeen];
}

export default createAnnouncementsStore;
