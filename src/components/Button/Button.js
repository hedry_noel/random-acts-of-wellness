import {children} from "solid-js";

const Button = props => {
  const c = children(() => props.children);

  return <button class={`button u-py-4 u-px-4 u-shadow-sm ${props.class}`} type={props.type ?? 'button'} onClick={props.onClick}>{c()}</button>
}

export default Button;
