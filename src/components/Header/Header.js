import {Link, useNavigate} from "solid-app-router";
import Badge from "../Badge/Badge";

const Header = props => {
  const navigate = useNavigate();

  return (
    <header className="card header__main u-mb-5 u-pt-8 u-w-100 u-pointer">
      <div className="header__main--title u-m-auto u-pb-1">
        <h1 className="font-headline font-color-primary text-center uppercase"
            onClick={() => navigate('/')}>Random Acts of Wellness</h1>
      </div>
      <nav class={"font-md u-flex u-flex-row u-align-center u-justify-center u-pb-5 u-print-none"}>
        <Link href="/">
          Home
        </Link>
        <Link class="u-ml-5" href="/history">
          History
        </Link>
        <Link class="u-ml-5" href="/announcements">
          <span class={"u-flex u-flex-row"}>
            Announcements
            {props.announcementCount && <Badge class={"font-sm u-ml-2"}>{props.announcementCount}</Badge>}
          </span>
        </Link>
      </nav>
    </header>
  )
}

export default Header;
