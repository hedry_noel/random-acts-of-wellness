import {createTaskStorage} from "../../hooks/createTaskStorage";
import Card from "../../containers/Card";
import {createRandomTask} from "../../services/wellnessTasks";
import {taskToStorage} from "../../services/util";
import {pipe} from "ramda";

const Randomizer = (props) => {
  const {updateTask} = createTaskStorage();

  const handleClick = () => {
    const newTask = createRandomTask();
    props.setTask(newTask);
    pipe(
      taskToStorage,
      updateTask
    )(newTask)
  }
  return (
    <Card class={'randomizer font-base font-md u-m-auto u-my-5 u-py-5 u-px-5'}>
      <p class={"text-center"}>I don't like that task! Give me a new one!</p>
      <button class={'button u-mt-4 u-py-4 u-px-3 u-shadow-sm'} onClick={handleClick}>Randomize!</button>
    </Card>
  )
}

export default Randomizer;
